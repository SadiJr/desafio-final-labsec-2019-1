# Desafio-Final-LabSEC-2019-1

Última etapa do processo seletivo de 2019/1 para ingressar no PBAD/LabSEC. O mesmo consiste em implementar uma aplicação capaz de realizar algumas operações comuns à segurança da computação, sendo elas:
* obter o resumo criptográfico de um documento;
* gerar chaves;
* gerar certificados;
* gerar repositório de chaves;
* gerar uma assinatura digital;
* verificar uma assinatura digital.

**OBS:** Devido ao diretório *.git* ter totalizado um tamanho de cerca de 5 MiB e, dado que o tamanho máximo de upload é de no máximo 2 MiB, tal diretório foi retirado da versão enviada.

Caso deseje visualizar a evolução do processo através dos commits, basta clicar [aqui](https://gitlab.com/SadiJr/desafio-final-labsec-2019-1).

**OBS2:** Ocorre um bug quando o projeto é colocado em um diretório com acentuação ou com espaços no nome, por isso recomendo colocar o arquivo em um diretório que não possua essas características (como */home/user/projects/*, por exemplo).
