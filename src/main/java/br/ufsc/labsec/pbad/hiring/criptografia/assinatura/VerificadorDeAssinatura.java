package br.ufsc.labsec.pbad.hiring.criptografia.assinatura;

import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationVerifier;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;

/**
 * Classe responsável por verificar a integridade de uma assinatura.
 */
public class VerificadorDeAssinatura {

	/**
	 * Provedor usado para verificar uma assinatura.
	 */
	private final String provider = "BC";

	/**
	 * Construtor.
	 * <p>
	 * O provedor do Bouncy Castle é adicionado para prover os serviços necessários
	 * à geração de assinaturas.
	 */
	public VerificadorDeAssinatura() {
		Security.addProvider(new BouncyCastleProvider());
	}

	/**
	 * Verifica a integridade de uma assinatura digital no padrão CMS.
	 *
	 * @param certificado certificado do assinante.
	 * @param assinatura  documento assinado.
	 * @return {@code true} se a assinatura for íntegra, e {@code false} do
	 *         contrário.
	 * @throws OperatorCreationException
	 * @throws CMSException
	 * @throws CertificateException 
	 */
	public boolean verificarAssinatura(X509Certificate certificado, CMSSignedData assinatura)
			throws OperatorCreationException, CMSException, CertificateException {
		SignerInformation verifier = pegaInformacoesAssinatura(assinatura);
		SignerInformationVerifier signature = geraVerificadorInformacoesAssinatura(certificado);
		return verifier.verify(signature);
	}

	/**
	 * Gera o verificador de assinaturas a partir das informações do assinante.
	 *
	 * @param certificado certificado do assinante.
	 * @return Objeto que representa o verificador de assinaturas.
	 * @throws OperatorCreationException
	 */
	private SignerInformationVerifier geraVerificadorInformacoesAssinatura(X509Certificate certificado)
			throws OperatorCreationException {
		return new JcaSimpleSignerInfoVerifierBuilder().setProvider(provider).build(certificado);
	}

	/**
	 * Classe responsável por pegar as informações da assinatura dentro do CMS.
	 *
	 * @param assinatura documento assinado.
	 * @return Informações da assinatura.
	 */
	private SignerInformation pegaInformacoesAssinatura(CMSSignedData assinatura) {
		return (SignerInformation) assinatura.getSignerInfos().getSigners().toArray()[0];
	}
}