package br.ufsc.labsec.pbad.hiring.criptografia.certificado;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.util.encoders.Base64;

/**
 * Classe responsável por ler um certificado do disco.
 *
 * @see CertificateFactory
 */
public class LeitorDeCertificados {
	
	/**
	 * Não usei essa variável pois não vi necessidade de utilizar a mesma em nenhum momento.
	 * Sendo sincero, também não faço a menor ideia de para o quê ela serve...
	 */
	private static final String X509Certificate = null;

	/**
	 * Lê um certificado no local indicado.
	 *
	 * @param caminhoCertificado caminho do certificado a ser lido.
	 *
	 * @return Objeto do certificado.
	 * @throws IOException
	 * @throws CertificateException
	 */
	public static X509Certificate lerCertificadoDoDisco(String caminhoCertificado) throws CertificateException, IOException {
		CertificateFactory factory = CertificateFactory.getInstance("X.509");
		byte[] decode = FileUtils.readFileToByteArray(new File(caminhoCertificado));
		return (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(Base64.decode(decode)));
	}
}