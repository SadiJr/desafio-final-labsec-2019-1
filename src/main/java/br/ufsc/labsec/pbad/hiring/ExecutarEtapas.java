package br.ufsc.labsec.pbad.hiring;

import br.ufsc.labsec.pbad.hiring.etapas.PrimeiraEtapa;
import br.ufsc.labsec.pbad.hiring.etapas.QuartaEtapa;
import br.ufsc.labsec.pbad.hiring.etapas.QuintaEtapa;
import br.ufsc.labsec.pbad.hiring.etapas.SegundaEtapa;
import br.ufsc.labsec.pbad.hiring.etapas.SextaEtapa;
import br.ufsc.labsec.pbad.hiring.etapas.TerceiraEtapa;

/**
 * Classe principal, responsável por executar todas as etapas.
 */

public class ExecutarEtapas {
    public static void main(String[] args) {
    	try {
    		new PrimeiraEtapa().executarEtapa();
    		new SegundaEtapa().executarEtapa();
    		new TerceiraEtapa().executarEtapa();
    		new QuartaEtapa().executarEtapa();
    		new QuintaEtapa().executarEtapa();
    		new SextaEtapa().executarEtapa();
    	} catch (Exception e) {
    		System.out.println("O desafio está apresentando erros!");
    		e.printStackTrace();
		}
    }
}