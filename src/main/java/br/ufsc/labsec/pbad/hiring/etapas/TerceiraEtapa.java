package br.ufsc.labsec.pbad.hiring.etapas;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.spec.InvalidKeySpecException;

import org.apache.commons.codec.DecoderException;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.jce.provider.X509CertificateObject;

import br.ufsc.labsec.pbad.hiring.criptografia.assimetrica.LeitorDeChaves;
import br.ufsc.labsec.pbad.hiring.criptografia.certificado.EscritorDeCertificados;
import br.ufsc.labsec.pbad.hiring.criptografia.certificado.GeradorDeCertificados;

/**
 * <b>Terceira etapa - gerar certificados digitais</b>
 * <p>
 * Aqui você terá que gerar dois certificados digitais. A identidade ligada a um
 * dos certificados digitais deverá ser a sua. A entidade emissora do seu
 * certificado será a AC-Raiz, a qual a chave privada já foi previamente gerada.
 * Também deverá ser feito o certificado digital para a AC-Raiz, certificado
 * esse que deverá ser autoassinado.
 * <p>
 * Os pontos a serem verificados para essa etapa ser considerada concluída são
 * os seguintes:
 * <ul>
 * <li>Emitir um certificado digital no formato X.509 para a AC-Raiz.
 * autoassinado;</li>
 * <li>Emitir um certificado digital no formato X.509, assinado pela AC-Raiz. O
 * certificado deve ter as seguintes características:
 * <ul>
 * <li>{@code Subject} deverá ser o seu nome;</li>
 * <li>{@code SerialNumber} deverá ser o número da sua matrícula;</li>
 * <li>{@code Issuer} deverá ser a AC-Raiz.</li>
 * </ul>
 * </li>
 * <li>Anexar ao desafio os certificados emitidos (extensão {@code .pem} ou
 * {@code .cer});</li>
 * <li>As chaves utilizadas nessa etapa deverão ser as mesmas já geradas.</li>
 * </ul>
 * <p>
 * Todas as variáveis globais definidas nessa classe devem ser usadas. Elas
 * definem os locais para escrever os resultados obtidos.
 */
public class TerceiraEtapa {

	private static final int numeroDeSerie = 17100922;
	private static final String nomeUsuario = "CN=Sadi-Jr";

	private static final String caminhoChavePublicaUsuario = "artefatos/chaves/chavePublicaUsuario";
	private static final String caminhoCertificadoUsuario = "artefatos/certificadoUsuario/certificadoUsuario.pem";

	private static final int numeroSerieAc = 1;
	private static final String nomeAcRaiz = "CN=AC-RAIZ";

	private static final String caminhoChavePublicaAc = "artefatos/chaves/chavePublicaAcRaiz";
	private static final String caminhoChavePrivadaAc = "artefatos/chaves/chavePrivadaAcRaiz";

	private static final String caminhoCertificadoAc = "artefatos/certificadoAcRaiz/certificadoAcRaiz.pem";

	public void executarEtapa()
			throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, InvalidKeyException, SignatureException, CertificateException, DecoderException {
		GeradorDeCertificados generator = new GeradorDeCertificados();

		String pathAcCertificate = getClass().getClassLoader().getResource(caminhoCertificadoAc).getFile();
		X509CertificateObject createAcCertificate = createAcCertificate(generator);
		EscritorDeCertificados.escreveCertificado(pathAcCertificate, createAcCertificate.getEncoded());

		String pathUserCertificate = getClass().getClassLoader().getResource(caminhoCertificadoUsuario).getFile();
		X509CertificateObject createUserCertificate = createUserCertificate(generator);
		EscritorDeCertificados.escreveCertificado(pathUserCertificate, createUserCertificate.getEncoded());
	}

	private X509CertificateObject createAcCertificate(GeradorDeCertificados generator)
			throws CertificateParsingException, NoSuchAlgorithmException, InvalidKeySpecException, IOException,
			InvalidKeyException, SignatureException {
		String pathPublicKeyAc = getClass().getClassLoader().getResource(caminhoChavePublicaAc).getFile();
		String pathPrivateKeyAc = getClass().getClassLoader().getResource(caminhoChavePrivadaAc).getFile();

		PublicKey publicKeyAc = LeitorDeChaves.lerChavePublicaDoDisco(pathPublicKeyAc, "RSA");
		PrivateKey privateKeyAc = LeitorDeChaves.lerChavePrivadaDoDisco(pathPrivateKeyAc, "RSA");

		TBSCertificate certificateStructAc = generator.gerarEstruturaCertificado(publicKeyAc, numeroSerieAc, nomeAcRaiz,
				nomeAcRaiz, 1);
		DERBitString signatureAc = generator.geraValorDaAssinaturaCertificado(certificateStructAc, privateKeyAc);
		return generator.gerarCertificado(certificateStructAc,
				new AlgorithmIdentifier(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId())),
				signatureAc);
	}
	
	private X509CertificateObject createUserCertificate(GeradorDeCertificados generator)
			throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, InvalidKeyException, SignatureException, CertificateParsingException {
		String pathPublicKeyUser = getClass().getClassLoader().getResource(caminhoChavePublicaUsuario).getFile();
		String pathPrivateKeyAc = getClass().getClassLoader().getResource(caminhoChavePrivadaAc).getFile();
		
		PublicKey publicKeyUser = LeitorDeChaves.lerChavePublicaDoDisco(pathPublicKeyUser, "RSA");
		PrivateKey privateKeyAc = LeitorDeChaves.lerChavePrivadaDoDisco(pathPrivateKeyAc, "RSA");
		
		TBSCertificate certificateStructUser = generator.gerarEstruturaCertificado(publicKeyUser, numeroDeSerie, nomeUsuario,
				nomeAcRaiz, 1);
		DERBitString signatureUser = generator.geraValorDaAssinaturaCertificado(certificateStructUser, privateKeyAc);
		return generator.gerarCertificado(certificateStructUser,
				new AlgorithmIdentifier(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId())), signatureUser);
	}
}