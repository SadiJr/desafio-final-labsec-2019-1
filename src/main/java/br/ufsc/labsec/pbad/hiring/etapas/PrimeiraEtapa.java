package br.ufsc.labsec.pbad.hiring.etapas;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

import br.ufsc.labsec.pbad.hiring.criptografia.resumo.Sha1;

/**
 * <b>Primeira etapa - obter o resumo criptográfico de um documento</b>
 * <p>
 * Aqui a tarefa é bem simples, pois basta obter o resumo criptográfico do
 * documento {@code textoPlano.txt}.
 * <p>
 * Os pontos a serem verificados para essa etapa ser considerada concluída
 * são os seguintes:
 * <ul>
 * <li>
 * Obter o resumo criptográfico do documento, especificado na descrição
 * dessa etapa, usando o algoritmo de resumo criptográfico conhecido por SHA-1;
 * </li>
 * <li>
 * Anexar ao desafio o documento contendo o resultado do resumo criptográfico.
 * </li>
 * </ul>
 * <p>
 * Todas as variáveis globais definidas nessa classe devem ser usadas.
 * Elas definem os locais para escrever os resultados obtidos.
 */
public class PrimeiraEtapa {

    private static final String caminhoTextoPlano = "artefatos/textos/textoPlano.txt";
    private static final String caminhoResumoCriptografico = "artefatos/resumos/resumoTextoPlano";
    
    public void executarEtapa() throws IOException, NoSuchAlgorithmException {
    	File file = new File(getClass().getClassLoader().getResource(caminhoTextoPlano).getFile());
    	Sha1 sha1 = new Sha1();
    	byte[] resumir = sha1.resumir(file);
    	byte[] encrypted = new String(Hex.encodeHex(resumir)).getBytes();
    	String pathToHashFile = getClass().getClassLoader().getResource(caminhoResumoCriptografico).getFile();
    	sha1.escreveResumoEmDisco(encrypted, pathToHashFile);
    }
}
