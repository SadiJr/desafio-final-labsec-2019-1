package br.ufsc.labsec.pbad.hiring.etapas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.operator.OperatorCreationException;

import br.ufsc.labsec.pbad.hiring.criptografia.assinatura.GeradorDeAssinatura;
import br.ufsc.labsec.pbad.hiring.criptografia.pkcs12.Pkcs12;

/**
 * <b>Quinta etapa - gerar uma assinatura digital</b>
 * <p>
 * Essa etapa é um pouco mais complexa, pois será necessário que
 * implemente um método para gerar assinaturas digitais. O padrão de
 * assinatura digital adotado será o Cryptographic Message Syntax (CMS).
 * <p>
 * Esse padrão usa a linguagem ASN.1, que é uma notação em binário, assim
 * não será possível ler o resultado obtido sem o auxílio de alguma
 * ferramenta. Caso tenha interesse em ver a estrutura da assinatura
 * gerada, recomenda-se o uso da ferramenta {@code dumpasn1}.
 * <p>
 * Os pontos a serem verificados para essa etapa ser considerada concluída
 * são os seguintes:
 * <ul>
 * <li>
 * Gerar um assinatura digital usando o algoritmo de resumo criptográfico
 * SHA-1 e o algoritmo assimétrico RSA;
 * </li>
 * <li>
 * O assinante será você. Então, use o PKCS#12 recém gerado para seu
 * certificado e chave privada;
 * </li>
 * <li>
 * Assinar o documento {@code textoPlano.txt};
 * </li>
 * <li>
 * Gravar a assinatura em disco.
 * </li>
 * </ul>
 * <p>
 * Todas as variáveis globais definidas nessa classe devem ser usadas.
 * Elas definem os locais para escrever os resultados obtidos.
 */
public class QuintaEtapa {

    private static final String caminhoPkcs12Usuario = "artefatos/certificadoUsuario/pkcs12Usuario.p12";
    private static final String aliasUsuario = "usuario";
    private static final char[] senhaMestre = "1234".toCharArray();

    private static final String caminhoTextoPlano = "artefatos/textos/textoPlano.txt";

    private static final String caminhoAssinatura = "artefatos/assinaturas/assinatura.der";

	public void executarEtapa() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException,
			UnrecoverableKeyException, OperatorCreationException, CMSException, InvalidKeyException, NoSuchProviderException, SignatureException {
    	Pkcs12 pkcs12 = new Pkcs12();
    	pkcs12.informarAlias(aliasUsuario);
    	pkcs12.informarSenha(senhaMestre);
    	pkcs12.informarCaminho(caminhoPkcs12Usuario);
    	pkcs12.abrir(getClass().getClassLoader().getResource(caminhoPkcs12Usuario).getFile());
    	
    	GeradorDeAssinatura geradorDeAssinatura = new GeradorDeAssinatura();
    	geradorDeAssinatura.informaAssinante(pkcs12.pegarCertificado(), pkcs12.pegarChavePrivada());
    	CMSSignedData assinatura = geradorDeAssinatura.assinar(getClass().getClassLoader().getResource(caminhoTextoPlano).getFile());
    	geradorDeAssinatura.escreveAssinatura(new FileOutputStream(new File(getClass().getClassLoader().getResource(caminhoAssinatura).getFile())), assinatura);
    }

}
