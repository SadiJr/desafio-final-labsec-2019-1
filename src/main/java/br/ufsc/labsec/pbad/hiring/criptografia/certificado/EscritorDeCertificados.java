package br.ufsc.labsec.pbad.hiring.criptografia.certificado;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

/**
 * Classe responsável por escrever um certificado no disco.
 */
public class EscritorDeCertificados {

    /**
     * Escreve o certificado indicado no disco.
     *
     * @param nomeArquivo           caminho que será escrito o certificado.
     * @param certificadoCodificado bytes do certificado.
     * @throws IOException 
     * @throws DecoderException 
     */
    public static void escreveCertificado(String nomeArquivo,
                                          byte[] certificadoCodificado) throws IOException, DecoderException {
    	FileUtils.writeByteArrayToFile(new File(nomeArquivo), Base64.encodeBase64(certificadoCodificado));
    	
    }
}