package br.ufsc.labsec.pbad.hiring.criptografia.assimetrica;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

/**
 * Classe responsável por gerar pares de chaves assimétricas.
 *
 * @see KeyPair
 * @see PublicKey
 * @see PrivateKey
 */
public class GeradorDeChaves {

    private String algoritmo;
    private KeyPairGenerator generator;

    /**
     * Construtor.
     *
     * @param algoritmo algoritmo de criptografia assimétrica a ser usado.
     */
    public GeradorDeChaves(String algoritmo) {
    	this.algoritmo = algoritmo;
    }

    /**
     * Gera um par de chaves, usando o algoritmo definido pela classe, com o
     * tamanho da chave especificado.
     *
     * @param tamanhoDaChave tamanho em bits das chaves geradas.
     * @return Par de chaves.
     * @throws NoSuchAlgorithmException 
     * @see SecureRandom
     */
    public KeyPair gerarParDeChaves(int tamanhoDaChave) throws NoSuchAlgorithmException {
    	generator = KeyPairGenerator.getInstance(this.algoritmo);
    	generator.initialize(tamanhoDaChave);
    	return generator.generateKeyPair();
    }
}