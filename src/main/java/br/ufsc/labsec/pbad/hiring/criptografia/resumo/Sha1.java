package br.ufsc.labsec.pbad.hiring.criptografia.resumo;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.io.FileUtils;

/**
 * Classe responsável por executar a função de resumo criptográfico.
 *
 * @see MessageDigest
 */
public class Sha1 {

    private final String algoritmo = "sha-1";
    private MessageDigest md;

    public Sha1() {
    }

    /**
     * Calcula o resumo criptográfico do arquivo indicado.
     *
     * @param arquivoDeEntrada arquivo a ser processado.
     * @return Bytes do resumo.
     * @throws NoSuchAlgorithmException 
     * @throws IOException 
     */
    public byte[] resumir(File arquivoDeEntrada) throws NoSuchAlgorithmException, IOException {
    	md = MessageDigest.getInstance(algoritmo);
    	byte[] file = FileUtils.readFileToByteArray(arquivoDeEntrada);
    	md.update(file);
    	return md.digest();
    }

    /**
     * Escreve o resumo criptográfico no local indicado.
     *
     * @param resumo         resumo criptográfico em bytes.
     * @param caminhoArquivo caminho do arquivo.
     * @throws IOException 
     */
    public void escreveResumoEmDisco(byte[] resumo, String caminhoArquivo) throws IOException {
    	FileUtils.writeByteArrayToFile(new File(caminhoArquivo), resumo);
    }
}