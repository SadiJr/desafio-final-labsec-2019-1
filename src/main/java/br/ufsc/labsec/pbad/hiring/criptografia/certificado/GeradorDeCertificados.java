package br.ufsc.labsec.pbad.hiring.criptografia.certificado;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateParsingException;
import java.util.Date;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.asn1.x509.Time;
import org.bouncycastle.asn1.x509.V3TBSCertificateGenerator;
import org.bouncycastle.jce.provider.X509CertificateObject;

/**
 * Classe responsável por gerar certificados no padrão X.509.
 * <p>
 * Um certificado é basicamente composto por três partes, que são:
 * <ul>
 * <li>Estrutura de informações do certificado;</li>
 * <li>Algoritmo de assinatura;</li>
 * <li>Valor da assinatura.</li>
 * </ul>
 */

public class GeradorDeCertificados {

	/**
	 * Gera a estrutura de informações de um certificado.
	 *
	 * @param chavePublica  chave pública do titular.
	 * @param numeroDeSerie número de série do certificado.
	 * @param nome          nome do titular.
	 * @param nomeAc        nome da autoridade emissora.
	 * @param dias          a partir da data atual, quantos dias de validade terá o
	 *                      certificado.
	 * @return Estrutura de informações do certificado.
	 * @throws IOException
	 */
	public TBSCertificate gerarEstruturaCertificado(PublicKey chavePublica, int numeroDeSerie, String nome,
			String nomeAc, int dias) throws IOException {
		V3TBSCertificateGenerator generator = new V3TBSCertificateGenerator();
		generator.setSerialNumber(new ASN1Integer(numeroDeSerie));
		generator.setIssuer(new X500Name(nome));
		long date = System.currentTimeMillis();
		generator.setStartDate(new Time(new Date(date)));
		generator.setEndDate(new Time(new Date(date + dias * 86400000l)));
		generator.setSubject(new X500Name(nomeAc));
		generator.setSubjectPublicKeyInfo(SubjectPublicKeyInfo.getInstance(chavePublica.getEncoded()));
		generator.setSignature(new AlgorithmIdentifier(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId())));
		return generator.generateTBSCertificate();
	}

	/**
	 * Gera valor da assinatura do certificado.
	 *
	 * @param estruturaCertificado estrutura de informações do certificado.
	 * @param chavePrivadaAc       chave privada da AC que emitirá esse certificado.
	 * @return Bytes da assinatura.
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws IOException 
	 * @throws SignatureException 
	 */
	public DERBitString geraValorDaAssinaturaCertificado(TBSCertificate estruturaCertificado,
			PrivateKey chavePrivadaAc) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException {
		Signature signature = Signature.getInstance("SHA512withRSA");
		signature.initSign(chavePrivadaAc);
		signature.update(estruturaCertificado.getEncoded(ASN1Encoding.DER));
		
		return new DERBitString(signature.sign());
	}

	/**
	 * Gera um certificado.
	 *
	 * @param estruturaCertificado  estrutura de informações do certificado.
	 * @param algoritmoDeAssinatura algoritmo de assinatura.
	 * @param valorDaAssinatura     valor da assinatura.
	 * @return Objeto que representa o certificado.
	 * @throws CertificateParsingException
	 * @see ASN1EncodableVector
	 */
	public X509CertificateObject gerarCertificado(TBSCertificate estruturaCertificado,
			AlgorithmIdentifier algoritmoDeAssinatura, DERBitString valorDaAssinatura)
			throws CertificateParsingException {
		ASN1EncodableVector encodableVector = new ASN1EncodableVector();
		encodableVector.add(estruturaCertificado);
		encodableVector.add(algoritmoDeAssinatura);
		encodableVector.add(valorDaAssinatura);

		return new X509CertificateObject(Certificate.getInstance(new DERSequence(encodableVector)));
	}
}