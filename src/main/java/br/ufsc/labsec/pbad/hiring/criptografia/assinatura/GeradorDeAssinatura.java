package br.ufsc.labsec.pbad.hiring.criptografia.assinatura;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DEROutputStream;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInfoGenerator;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;

/**
 * Classe responsável por gerar uma assinatura digital.
 * <p>
 * Aqui será necessário usar a biblioteca Bouncy Castle, pois ela já possui a
 * estrutura básica da assinatura implementada.
 */
public class GeradorDeAssinatura {

	/**
	 * Algoritmo usado para gerar assinaturas. Note que tem o algoritmo de resumo
	 * criptográfico junto com o de criptografia assimétrica.
	 */
	private final String algoritmoDeAssinatura = "SHA1withRSA";

	/**
	 * Provedor usado para verificar uma assinatura.
	 */
	private final String provider = "BC";

	private X509Certificate certificado;
	private PrivateKey chavePrivada;
	private CMSSignedDataGenerator geradorAssinaturaCms;

	/**
	 * Construtor.
	 * <p>
	 * O provedor do Bouncy Castle é adicionado para prover os serviços necessários
	 * à geração de assinaturas.
	 */
	public GeradorDeAssinatura() {
		Security.addProvider(new BouncyCastleProvider());
		geradorAssinaturaCms = new CMSSignedDataGenerator();
	}

	/**
	 * Informa qual será o assinante.
	 *
	 * @param certificado  certificado, no padrão X.509, do assinante.
	 * @param chavePrivada chave privada do assinante.
	 */
	public void informaAssinante(X509Certificate certificado, PrivateKey chavePrivada) {
		this.certificado = certificado;
		this.chavePrivada = chavePrivada;
	}

	/**
	 * Gera uma assinatura no padrão CMS, com os dados anexados a assinatura. Isso
	 * quer dizer que a assinatura contém os dados assinados dentro de sua
	 * estrutura.
	 *
	 * @param caminhoDocumento caminho do documento que será assinado.
	 * @return Documento assinado.
	 * @throws IOException
	 * @throws OperatorCreationException
	 * @throws CertificateEncodingException
	 * @throws CMSException
	 * @throws NoSuchProviderException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws SignatureException
	 */
	public CMSSignedData assinar(String caminhoDocumento)
			throws IOException, OperatorCreationException, CertificateEncodingException, CMSException,
			NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
		CMSTypedData message = preparaDadosParaAssinar(caminhoDocumento);
		ArrayList<X509Certificate> certificates = new ArrayList<X509Certificate>();
		certificates.add(certificado);
		JcaCertStore jcaCertStore = new JcaCertStore(certificates);

		geradorAssinaturaCms.addSignerInfoGenerator(preparaInformacoesAssinante(chavePrivada, certificado));

		geradorAssinaturaCms.addCertificates(jcaCertStore);
		return geradorAssinaturaCms.generate(message, true);
	}

	/**
	 * Transforma o documento que será assinado.
	 *
	 * @param caminhoDocumento caminho do documento que será assinado.
	 * @return Documento no formato correto.
	 * @throws IOException
	 * @throws NoSuchProviderException
	 * @throws NoSuchAlgorithmException
	 * @throws SignatureException
	 * @throws InvalidKeyException
	 */
	private CMSTypedData preparaDadosParaAssinar(String caminhoDocumento) throws IOException, NoSuchAlgorithmException,
			NoSuchProviderException, SignatureException, InvalidKeyException {
		byte[] file = FileUtils.readFileToByteArray(new File(caminhoDocumento));
		Signature signature = Signature.getInstance(algoritmoDeAssinatura, provider);
		signature.initSign(chavePrivada);
		signature.update(file);

		return new CMSProcessableByteArray(signature.sign());
	}

	/**
	 * Gera as informações do assinante na estrutura necessária para ser adicionada
	 * na assinatura.
	 *
	 * @param chavePrivada chave privada do assinante.
	 * @param certificado  certificado do assinante.
	 * @return Estrutura com informações do assinante.
	 * @throws OperatorCreationException
	 * @throws CertificateEncodingException
	 */
	private SignerInfoGenerator preparaInformacoesAssinante(PrivateKey chavePrivada, Certificate certificado)
			throws CertificateEncodingException, OperatorCreationException {
		return new JcaSimpleSignerInfoGeneratorBuilder().setProvider(provider).build(algoritmoDeAssinatura,
				chavePrivada, (X509Certificate) certificado);
	}

	/**
	 * Escreve a assinatura no local apontado.
	 *
	 * @param arquivo    arquivo que será escrita a assinatura.
	 * @param assinatura objeto da assinatura.
	 * @throws IOException
	 */
	public void escreveAssinatura(OutputStream arquivo, CMSSignedData assinatura) throws IOException {
		ASN1InputStream asn1 = new ASN1InputStream(assinatura.getEncoded());
		FileOutputStream file = (FileOutputStream) arquivo;
		DEROutputStream writer = new DEROutputStream(file);
		writer.writeObject(asn1.readObject());

		writer.flush();
		writer.close();
		asn1.close();
	}
}