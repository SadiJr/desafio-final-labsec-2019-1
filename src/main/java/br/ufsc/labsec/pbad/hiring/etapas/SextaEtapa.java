package br.ufsc.labsec.pbad.hiring.etapas;

import java.io.File;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.operator.OperatorCreationException;

import br.ufsc.labsec.pbad.hiring.criptografia.assinatura.VerificadorDeAssinatura;
import br.ufsc.labsec.pbad.hiring.criptografia.pkcs12.Pkcs12;

/**
 * <b>Sexta etapa - verificar uma assinatura digital</b>
 * <p>
 * Por último, será necessário verificar a integridade da assinatura recém
 * gerada. Note que o processo de validação de uma assinatura digital pode ser
 * muito complexo, mas aqui o desafio será simples. Para verificar a assinatura
 * será necessário apenas decifrar o valor da assinatura (resultante do processo
 * de cifra do resumo criptográfico do arquivo {@code textoPlano.txt} com as
 * informações da estrutura da assinatura) e comparar esse valor com o valor do
 * resumo criptográfico do arquivo assinado. Como dito na fundamentação, para
 * assinar é usada a chave privada, e para decifrar (verificar) é usada a chave
 * pública.
 * <p>
 * Os pontos a serem verificados para essa etapa ser considerada concluída são
 * os seguintes:
 * <ul>
 * <li>Verificar a assinatura gerada na etapa anterior, de acordo com o processo
 * descrito, e apresentar esse resultado.</li>
 * </ul>
 * <p>
 * Todas as variáveis globais definidas nessa classe devem ser usadas. Elas
 * definem os locais para escrever os resultados obtidos.
 */
public class SextaEtapa {

	private static final String caminhoPkcs12Usuario = "artefatos/certificadoUsuario/pkcs12Usuario.p12";
	private static final String aliasUsuario = "usuario";
	private static final char[] senhaMestre = "1234".toCharArray();

	private static final String caminhoAssinatura = "artefatos/assinaturas/assinatura.der";

	public void executarEtapa() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException,
			OperatorCreationException, CMSException {
		Pkcs12 pkcs12 = new Pkcs12();
		pkcs12.informarAlias(aliasUsuario);
		pkcs12.informarSenha(senhaMestre);
		pkcs12.informarCaminho(caminhoPkcs12Usuario);
		pkcs12.abrir(getClass().getClassLoader().getResource(caminhoPkcs12Usuario).getFile());
		
		byte[] file = FileUtils.readFileToByteArray(new File(getClass().getClassLoader().getResource(caminhoAssinatura).getFile()));
		VerificadorDeAssinatura signatureVerifier = new VerificadorDeAssinatura();
		boolean verificarAssinatura = signatureVerifier.verificarAssinatura(pkcs12.pegarCertificado(), new CMSSignedData(file));
		System.out.println(verificarAssinatura);
	}

}
