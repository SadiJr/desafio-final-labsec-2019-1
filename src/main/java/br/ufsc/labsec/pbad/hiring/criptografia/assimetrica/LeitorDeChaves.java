package br.ufsc.labsec.pbad.hiring.criptografia.assimetrica;

import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.apache.commons.io.FileUtils;

/**
 * Classe responsável por ler uma chave assimétrica do disco.
 *
 * @see KeyFactory
 * @see KeySpec
 */
public class LeitorDeChaves {

	/**
	 * Lê a chave privada do local indicado.
	 *
	 * @param caminhoChave local do arquivo da chave privada.
	 * @param algoritmo    algoritmo de criptografia assimétrica que a chave foi
	 *                     gerada.
	 * @return Chave privada.
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	public static PrivateKey lerChavePrivadaDoDisco(String caminhoChave, String algoritmo)
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] file = FileUtils.readFileToByteArray(new File(caminhoChave));
		PKCS8EncodedKeySpec reader = new PKCS8EncodedKeySpec(file);
		KeyFactory decryptor = KeyFactory.getInstance(algoritmo);
		return decryptor.generatePrivate(reader);
	}

	/**
	 * Lê a chave pública do local indicado.
	 *
	 * @param caminhoChave local do arquivo da chave pública.
	 * @param algoritmo    algoritmo de criptografia assimétrica que a chave foi
	 *                     gerada.
	 * @return Chave pública.
	 * @throws IOException
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	public static PublicKey lerChavePublicaDoDisco(String caminhoChave, String algoritmo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] file = FileUtils.readFileToByteArray(new File(caminhoChave));
		X509EncodedKeySpec reader = new X509EncodedKeySpec(file);
		KeyFactory decryptor = KeyFactory.getInstance(algoritmo);
		return decryptor.generatePublic(reader);
	}
}