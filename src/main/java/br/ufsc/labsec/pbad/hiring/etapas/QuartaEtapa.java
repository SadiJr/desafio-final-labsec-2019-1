package br.ufsc.labsec.pbad.hiring.etapas;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;

import br.ufsc.labsec.pbad.hiring.criptografia.assimetrica.LeitorDeChaves;
import br.ufsc.labsec.pbad.hiring.criptografia.certificado.LeitorDeCertificados;
import br.ufsc.labsec.pbad.hiring.criptografia.pkcs12.GeradorDePkcs12;

/**
 * <b>Quarta etapa - gerar repositório de chaves seguro</b>
 * <p>
 * Essa etapa tem como finalidade gerar um repositório seguro de chaves
 * assimétricas. Esse repositório deverá ser no formato PKCS#12. Note que
 * esse repositório é basicamente um tabela hash com pequenas mudanças. Por
 * exemplo, sua estrutura seria algo como {@code <Alias, <Certificado, Chave
 * Privada>>}, onde o alias é um nome amigável dado a uma entrada da
 * estrutura, o certificado e chave privada devem ser correspondentes à
 * mesma identidade. O alias serve como elemento de busca dessa identidade.
 * O PKCS#12 ainda conta com uma senha, que serve para cifrar a estrutura
 * (isso é feito de modo automático).
 * <p>
 * Os pontos a serem verificados para essa etapa ser considerada concluída
 * são os seguintes:
 * <ul>
 * <li>
 * Gerar um PKCS#12 para o seu certificado/chave privada com senha e
 * alias de acordo com as variáveis globais descritas nessa classe;
 * </li>
 * <li>
 * Gerar um PKCS#12 para o certificado/chave privada da AC-Raiz. Usar
 * a senha e o alias descritos pelas variáveis globais dessa classe.
 * </li>
 * </ul>
 * <p>
 * Todas as variáveis globais definidas nessa classe devem ser usadas.
 * Elas definem os locais para escrever os resultados obtidos.
 */
public class QuartaEtapa {

    private static final String caminhoChavePrivadaUsuario = "artefatos/chaves/chavePrivadaUsuario";
    private static final String caminhoCertificadoUsuario = "artefatos/certificadoUsuario/certificadoUsuario.pem";

    private static final String caminhoPkcs12Usuario = "artefatos/certificadoUsuario/pkcs12Usuario.p12";
    private static final String aliasUsuario = "usuario";

    private static final String caminhoChavePrivadaAc = "artefatos/chaves/chavePrivadaAcRaiz";
    private static final String caminhoCertificadoAc = "artefatos/certificadoAcRaiz/certificadoAcRaiz.pem";

    private static final String caminhoPkcs12Ac = "artefatos/certificadoAcRaiz/pkcs12AcRaiz.p12";
    private static final String aliasAc = "acRaiz";

    private static final char[] senhaMestre = "1234".toCharArray();

    public void executarEtapa() throws CertificateException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, KeyStoreException {
    	createUserRepository();
    	createAcRepository();
    }
    
    private void createUserRepository() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, CertificateException, KeyStoreException {
    	String pathPrivateKeyUser = getClass().getClassLoader().getResource(caminhoChavePrivadaUsuario).getFile();
    	String pathCertificateUser = getClass().getClassLoader().getResource(caminhoCertificadoUsuario).getFile();
		PrivateKey privateKeyUser = LeitorDeChaves.lerChavePrivadaDoDisco(pathPrivateKeyUser, "RSA");
		X509Certificate certificateUser = LeitorDeCertificados.lerCertificadoDoDisco(pathCertificateUser);
		String pkcsPathUser = getClass().getClassLoader().getResource(caminhoPkcs12Usuario).getFile();
		
    	GeradorDePkcs12.gerarPkcs12(privateKeyUser, certificateUser, pkcsPathUser, aliasUsuario, senhaMestre);
    }
    
    private void createAcRepository() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, CertificateException, KeyStoreException {
    	String pathPrivateKeyAc = getClass().getClassLoader().getResource(caminhoChavePrivadaAc).getFile();
    	String pathCertificateAc = getClass().getClassLoader().getResource(caminhoCertificadoAc).getFile();
		PrivateKey privateKeyAc = LeitorDeChaves.lerChavePrivadaDoDisco(pathPrivateKeyAc, "RSA");
		X509Certificate certificateAc = LeitorDeCertificados.lerCertificadoDoDisco(pathCertificateAc);
		String pkcsPathAc = getClass().getClassLoader().getResource(caminhoPkcs12Ac).getFile();
		
    	GeradorDePkcs12.gerarPkcs12(privateKeyAc, certificateAc, pkcsPathAc, aliasAc, senhaMestre);
    }
}