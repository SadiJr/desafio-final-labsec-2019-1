package br.ufsc.labsec.pbad.hiring.criptografia.pkcs12;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Essa classe representa um repositório de chaves do tipo PKCS#12.
 *
 * @see KeyStore
 */
public class Pkcs12 {

    /**
     * Tipo do repositório de chaves.
     */
    private static final String pkcs12 = "PKCS12";

    private String caminhoPkcs12;
    private KeyStore repositorioPkcs12;
    private char[] senha;
    private String alias;
    private boolean estaVazio;

    /**
     * Construtor.
     */
    public Pkcs12() {
        // TODO implementar
    }

    /**
     * Abre o repositório do local indicado.
     *
     * @param caminhoRepositorio caminho do PKCS#12.
     * @throws KeyStoreException 
     * @throws IOException 
     * @throws FileNotFoundException 
     * @throws CertificateException 
     * @throws NoSuchAlgorithmException 
     */
    public void abrir(String caminhoRepositorio) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
    	KeyStore store = KeyStore.getInstance(pkcs12);
    	store.load(new FileInputStream(caminhoRepositorio), senha);
    	this.repositorioPkcs12 = store;
    }

    /**
     * Obtém a chave privada do PKCS#12.
     *
     * @return Chave privada.
     * @throws NoSuchAlgorithmException 
     * @throws KeyStoreException 
     * @throws UnrecoverableKeyException 
     */
    public PrivateKey pegarChavePrivada() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
    	return (PrivateKey) repositorioPkcs12.getKey(alias, senha);
    }

    /**
     * Obtém do certificado do PKCS#12.
     *
     * @return Certificado.
     * @throws KeyStoreException 
     */
    public X509Certificate pegarCertificado() throws KeyStoreException {
    	return (X509Certificate) repositorioPkcs12.getCertificate(alias);
    }

    // Getters e setters que não precisam de documentação estão abaixo.

    public String pegarCaminho() {
        return this.caminhoPkcs12;
    }

    public void informarCaminho(String path) {
        this.caminhoPkcs12 = path;
    }

    public KeyStore pegarPkcs12() {
        return this.repositorioPkcs12;
    }

    public void informarPkcs12(KeyStore pkcs12) {
        this.repositorioPkcs12 = pkcs12;
    }

    public char[] pegarSenha() {
        return this.senha;
    }

    public void informarSenha(char[] password) {
        this.senha = password;
    }

    public String pegarAlias() {
        return this.alias;
    }

    public void informarAlias(String alias) {
        this.alias = alias;
    }

    public boolean estaVazio() {
        return this.estaVazio;
    }

    public void estaVazio(boolean isEmpty) {
        this.estaVazio = isEmpty;
    }

}
