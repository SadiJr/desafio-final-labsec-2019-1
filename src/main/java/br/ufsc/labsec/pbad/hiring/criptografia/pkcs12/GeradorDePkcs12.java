package br.ufsc.labsec.pbad.hiring.criptografia.pkcs12;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Classe responsável por gerar um PKCS#12.
 *
 * @see KeyStore
 */
public class GeradorDePkcs12 {

	/**
	 * Tipo do repositório de chaves.
	 */
	private static final String pkcs12 = "PKCS12";

	/**
	 * Gera um PKCS#12 para a chave privada/certificado passados como parâmetro.
	 *
	 * @param chavePrivada  chave privada do titular do certificado.
	 * @param certificado   certificado do titular.
	 * @param caminhoPkcs12 caminho onde será escrito o PKCS#12.
	 * @param alias         nome amigável dado à entrada do PKCS#12, que comportará
	 *                      a chave e o certificado.
	 * @param senha         senha de acesso ao PKCS#12.
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 */
	public static void gerarPkcs12(PrivateKey chavePrivada, X509Certificate certificado, String caminhoPkcs12,
			String alias, char[] senha)
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStore keyStore = KeyStore.getInstance(pkcs12);
		keyStore.load(null);

		keyStore.setEntry(alias, new KeyStore.PrivateKeyEntry(chavePrivada, new Certificate[] { certificado }),
				new KeyStore.PasswordProtection(senha));

		keyStore.store(new FileOutputStream(new File(caminhoPkcs12)), senha);
//		Pkcs12 pkcs122 = new Pkcs12();
//		pkcs122.informarAlias(alias);
//		pkcs122.informarCaminho(caminhoPkcs12);
//		pkcs122.informarPkcs12(keyStore);
//		pkcs122.informarSenha(senha);
	}
}