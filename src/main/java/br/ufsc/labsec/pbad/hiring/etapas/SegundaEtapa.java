package br.ufsc.labsec.pbad.hiring.etapas;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import br.ufsc.labsec.pbad.hiring.criptografia.assimetrica.EscritorDeChaves;
import br.ufsc.labsec.pbad.hiring.criptografia.assimetrica.GeradorDeChaves;

/**
 * <b>Segunda etapa - gerar chaves assimétricas</b>
 * <p>
 * A partir dessa etapa, tudo que será feito envolve criptografia assimétrica.
 * A tarefa aqui é parecida com a etapa anterior, pois refere-se apenas a
 * criar e armazenar chaves, mas nesse caso será usado apenas um algoritmo
 * de criptografia assimétrica, o RSA.
 * <p>
 * Os pontos a serem verificados para essa etapa ser considerada concluída
 * são os seguintes:
 * <ul>
 * <li>
 * Gerar um par de chaves usando o algoritmo RSA com o tamanho de 1024 bits;
 * </li>
 * <li>
 * Gerar outro par de chaves, mas com o tamanho de 2048 bits. Note que esse
 * par de chaves será para AC-Raiz;
 * </li>
 * <li>
 * Armazenar em disco os pares de chaves.
 * </li>
 * </ul>
 * <p>
 * Todas as variáveis globais definidas nessa classe devem ser usadas.
 * Elas definem os locais para escrever os resultados obtidos.
 */
public class SegundaEtapa {

    private static final String caminhoChavePublicaUsuario = "artefatos/chaves/chavePublicaUsuario";
    private static final String caminhoChavePrivadaUsuario = "artefatos/chaves/chavePrivadaUsuario";

    private static final String caminhoChavePublicaAc = "artefatos/chaves/chavePublicaAcRaiz";
    private static final String caminhoChavePrivadaAc = "artefatos/chaves/chavePrivadaAcRaiz";

    public void executarEtapa() throws IOException, NoSuchAlgorithmException {
    	GeradorDeChaves geradorDeChaves = new GeradorDeChaves("RSA");
    	
    	KeyPair keysUser = geradorDeChaves.gerarParDeChaves(1024);
    	
    	EscritorDeChaves.escreveChaveEmDisco(keysUser.getPublic(), getClass().getClassLoader().getResource(caminhoChavePublicaUsuario).getFile());
    	EscritorDeChaves.escreveChaveEmDisco(keysUser.getPrivate(), getClass().getClassLoader().getResource(caminhoChavePrivadaUsuario).getFile());
    	
    	KeyPair keysAc = geradorDeChaves.gerarParDeChaves(2048);
    	EscritorDeChaves.escreveChaveEmDisco(keysAc.getPublic(), getClass().getClassLoader().getResource(caminhoChavePublicaAc).getFile());
    	EscritorDeChaves.escreveChaveEmDisco(keysAc.getPrivate(), getClass().getClassLoader().getResource(caminhoChavePrivadaAc).getFile());
    }
}