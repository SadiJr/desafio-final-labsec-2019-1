package br.ufsc.labsec.pbad.hiring.criptografia.assimetrica;

import java.io.File;
import java.io.IOException;
import java.security.Key;

import org.apache.commons.io.FileUtils;

/**
 * Essa classe é responsável por escrever uma chave assimétrica no disco. Note
 * que a chave pode ser tanto uma chave pública quanto uma chave privada.
 *
 * @see Key
 */
public class EscritorDeChaves {

    /**
     * Escreve uma chave no local indicado.
     *
     * @param chave         chave assimétrica a ser escrita em disco.
     * @param nomeDoArquivo nome do local onde será escrita a chave.
     * @throws IOException 
     */
    public static void escreveChaveEmDisco(Key chave, String nomeDoArquivo) throws IOException {
    	FileUtils.writeByteArrayToFile(new File(nomeDoArquivo), chave.getEncoded());
    }
}